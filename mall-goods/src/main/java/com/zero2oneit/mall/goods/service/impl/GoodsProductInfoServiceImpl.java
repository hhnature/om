package com.zero2oneit.mall.goods.service.impl;

import com.zero2oneit.mall.common.bean.goods.GoodsProductInfo;
import com.zero2oneit.mall.goods.mapper.GoodsProductInfoMapper;
import com.zero2oneit.mall.goods.service.GoodsProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-02-22
 */
@Service
public class GoodsProductInfoServiceImpl extends ServiceImpl<GoodsProductInfoMapper, GoodsProductInfo> implements GoodsProductInfoService {

    @Autowired
    private GoodsProductInfoMapper goodsProductInfoMapper;

}