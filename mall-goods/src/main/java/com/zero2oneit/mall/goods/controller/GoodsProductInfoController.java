package com.zero2oneit.mall.goods.controller;

import com.zero2oneit.mall.common.bean.goods.GoodsCategory;
import com.zero2oneit.mall.goods.service.GoodsProductInfoRelationService;
import com.zero2oneit.mall.goods.service.GoodsProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Description: 商品管理（社区）
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-02-22
 */
@RestController
@RequestMapping("/admin/communityGoods")
public class GoodsProductInfoController {

    @Autowired
    private GoodsProductInfoService goodsProductService;

    @Autowired
    private GoodsProductInfoRelationService goodsRelationService;



}
