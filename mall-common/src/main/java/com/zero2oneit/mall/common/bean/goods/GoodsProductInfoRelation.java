package com.zero2oneit.mall.common.bean.goods;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-02-22
 */
@Data
@TableName("goods_product_info_relation")
public class GoodsProductInfoRelation implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 商品id,水平拆分
	 */
	@TableId
	private Long productId;
	/**
	 * 商品名称
	 */
	private String productName;
	/**
	 * 商品当前库存
	 */
	private Integer productStock;
	/**
	 * 已售数量
	 */
	private Integer soldStock;
	
}
